import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/mock/connect/list',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/mock/connect/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/mock/connect/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: '/mock/connect/create',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: '/mock/connect/update',
    method: 'post',
    data
  })
}
