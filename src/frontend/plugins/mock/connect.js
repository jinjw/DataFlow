const Mock = require('mockjs')

const List = []
const count = 50


for (let i = 0; i < count; i++) {
    List.push(Mock.mock({
      'type|1': ['File', 'DataBase', 'API'],
      title: '@title(1, 3)',
      createtime: '@datetime',
      'status|1': ['published', 'draft'],
       apisite: "http://"+'@title(1)'+'.com',
       'dbtype|1':['SQLSERVER','SQList','MySQL'],
       'dbserver':'192.168.8.1',
       dbuser:'@title(1, 1)',
       dbpwd:'@title(1, 1)',
       dbname:'@title(1, 1)',
       fileurl:'@title(1, 1)'
    }))
  }
  

  module.exports = [
    {
      url: '/mock/connect/list',
      type: 'get',
      response: config => {
        const { importance, type, title, page = 1, limit = 20, sort } = config.query
  
        let mockList = List.filter(item => {
          if (importance && item.importance !== +importance) return false
          if (type && item.type !== type) return false
          if (title && item.title.indexOf(title) < 0) return false
          return true
        })
  
        if (sort === '-id') {
          mockList = mockList.reverse()
        }
  
        const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))
  
        return {
          code: 20000,
          data: {
            total: mockList.length,
            items: pageList
          }
        }
      }
    },
  
    {
      url: '/mock/connect/detail',
      type: 'get',
      response: config => {
        const { id } = config.query
        for (const article of List) {
          if (article.id === +id) {
            return {
              code: 20000,
              data: article
            }
          }
        }
      }
    },
  
    {
      url: '/mock/connect/pv',
      type: 'get',
      response: _ => {
        return {
          code: 20000,
          data: {
            pvData: [
              { key: 'PC', pv: 1024 },
              { key: 'mobile', pv: 1024 },
              { key: 'ios', pv: 1024 },
              { key: 'android', pv: 1024 }
            ]
          }
        }
      }
    },
  
    {
      url: '/mock/connect/create',
      type: 'post',
      response: _ => {
        return {
          code: 20000,
          data: 'success'
        }
      }
    },
  
    {
      url: '/mock/connect/update',
      type: 'post',
      response: _ => {
        return {
          code: 20000,
          data: 'success'
        }
      }
    }
  ]
  