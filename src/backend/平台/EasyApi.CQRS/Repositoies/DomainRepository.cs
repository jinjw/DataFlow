﻿using EasyApi.CQRS.Domain;
using EasyApi.CQRS.Events;
using EasyApi.CQRS.Events.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyApi.CQRS.Repositoies
{
    public class DomainRepository<T> : IDomainRepository<T> where T : AggregateRoot,new()
    {
        private IEventStore _store = null;

        public DomainRepository(IEventStore store)
        {
            _store = store;
        }
        //在本次操作之前先加载出库中最新的聚合根实例，用于对比
        public T GetById(Guid id) {
            IEnumerable<DomainEvent> events = _store.GetEvents(id);
            var lastAggregate = new T();
            lastAggregate.LoadsFromHistory(events);
            return lastAggregate;
        }

        public void Save(T aggregateRoot)
        {
            var events = aggregateRoot.GetUnCommittedChanges();
            if (events.Any())
            {
                _store.Save( events);
            }
        }
    }
}
