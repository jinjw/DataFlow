﻿using EasyApi.CQRS.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Repositoies
{
    public interface IDomainRepository<T> where T:AggregateRoot
    {
        T GetById(Guid id);
        void Save(T aggregateRoot);
    }
}
