﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Entity
{
    [AttributeUsageAttribute(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class PrimaryKeyAttribute:Attribute
    {

    }
}
