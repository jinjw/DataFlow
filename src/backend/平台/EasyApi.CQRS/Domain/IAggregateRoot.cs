﻿using EasyApi.CQRS.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Domain
{
    public interface IAggregateRoot
    {
        IEnumerable<DomainEvent> GetUnCommittedChanges();
    }
}
