﻿using EasyApi.CQRS.Entity;
using EasyApi.CQRS.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace EasyApi.CQRS.Domain
{
    /// <summary>
    /// 聚合根是一个领域边界
    /// </summary>
    public class AggregateRoot :  IAggregateRoot
    {
        public Guid Id { get; set; }

        //聚合根当前的版本
        private long _version;
        //事件的版本
        private long _eventVersion;

        public long Version { get { return this._version; } }


        public readonly List<DomainEvent> _changes = new List<DomainEvent>();



        public BaseEntity MainEntity { get; set; }
        public List<BaseEntity> Entities { get; set; }


        public IEnumerable<DomainEvent> GetUnCommittedChanges()
        {
            return _changes;
        }


        private void CollectDomainEvent(BaseEntity target, BaseEntity source) {
           PropertyInfo[] props=target.GetType().GetProperties();
            foreach (var prop in props) {
                if (prop.GetValue(target) != prop.GetValue(source)) {
                    ApplyChange( new DomainEvent(this.Id, EventEnum.Update, this.Version, target),true);
                    return;
                }
            }
        }



        public void Compare(AggregateRoot newAggregateRoot) {
            CollectDomainEvent(newAggregateRoot.MainEntity, this.MainEntity);

            



            foreach (var entity in newAggregateRoot.Entities) {

                Type type = entity.GetType();
                var props = type.GetProperties();
                var prop = props.FirstOrDefault(p => p.IsDefined(typeof(PrimaryKeyAttribute)));
                if()
                this.Entities.Where(t => t.GetType() == type);


                //子类型是相同的
                CollectDomainEvent(entity, this.MainEntity);
            }
        }

        //用于装载历史的事件清单
        public void LoadsFromHistory(IEnumerable<DomainEvent> historyEvents)
        {
            foreach (var e in historyEvents)
                ApplyChange(e, false);

            _version = historyEvents.Last().Version;
            //EventVersion = Version;
        }

        public void ApplyChange(DomainEvent @event)
        {
            ApplyChange(@event, true);
        }

        private void ApplyChange(DomainEvent @event, bool isNew)
        {
            dynamic d = this;

            //d.Handle(TypeConverter.ChangeTo(@event, @event.GetType()));
            if (isNew)
            {
                _changes.Add(@event);
            }
        }

    }
}
