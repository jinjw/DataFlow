﻿using EasyApi.CQRS.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Events
{
    public enum EventEnum { 
        Insert,
        Update,
        Delete
    }


    public class DomainEvent:IDomainEvent
    {
        public EventEnum EventEnum { get { return _eventEnum; } }

        private EventEnum _eventEnum = EventEnum.Insert;

        public Guid Id { get; set; }

        private Guid _id = Guid.Empty;

        private long _version = 0;
        public long Version { get { return _version; } }

        private DateTime _createTime = DateTime.Now;
        public DateTime CreateTime { get { return _createTime; } }
        private BaseEntity _entity = null;
        public BaseEntity Source { get { return _entity; } set { this._entity = value; } }

        public DomainEvent(Guid id, EventEnum eventEnum, long version, BaseEntity entity)
        {
            _id = id;
            _version = version;
            _eventEnum = eventEnum;
            _entity = entity;
        }
        
       
    }
}
