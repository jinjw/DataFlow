﻿using EasyApi.CQRS.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Events.Storage
{
    public interface IEventStore
    {
        IEnumerable<DomainEvent> GetEvents(Guid aggregateId);
        void Save(IEnumerable<DomainEvent> events);
    }
}
