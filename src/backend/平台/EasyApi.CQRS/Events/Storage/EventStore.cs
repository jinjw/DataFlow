﻿using EasyApi.CQRS.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyApi.CQRS.Events.Storage
{
    public class EventStore : IEventStore
    {
        private readonly List<DomainEvent> _events;

        public IEnumerable<DomainEvent> GetEvents(Guid aggregateId)
        {
            var events = _events.Where(p => p.Id == aggregateId).Select(p => p);
            //if (!events.Any())
            //{
            //    throw new AggregateNotFoundException(string.Format("Aggregate with AggregateRootId: {0} was not found", aggregateId));
            //}
            return events;
        }

        public void Save(IEnumerable<DomainEvent> events)
        {

            foreach (var domainEvent in events) { 
               //save数据
            }

        }
    }
}
