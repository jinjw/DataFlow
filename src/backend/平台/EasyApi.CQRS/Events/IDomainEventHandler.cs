﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Events
{
    public interface IDomainEventHandler
    {
        void Handle(DomainEvent domainEvent);
    }
}
