﻿using EasyApi.CQRS.Domain;
using EasyApi.CQRS.Entity;
using EasyApi.CQRS.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Command
{
    /// <summary>
    /// 为了不增加很多细粒度的命令
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    [Serializable]
    public class Command<TEntity>:ICommand<TEntity> where TEntity:AggregateRoot
    {
        private Guid _id = Guid.Empty;

        private CommandEnum _commandEnum = default(CommandEnum);
        private TEntity _entity = null;
        public Command(Guid id,CommandEnum commandEnum,TEntity entity) {
            _id = id;
            _commandEnum = commandEnum;
            _entity = entity;
        }

        public Guid Id { get { return this._id; } }

        public TEntity AggregateRoot { get { return _entity; } }

        public CommandEnum CommandEnum { get { return _commandEnum; } }
    }
}
