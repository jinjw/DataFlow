﻿using EasyApi.CQRS.Domain;
using EasyApi.CQRS.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Command
{
    public interface ICommand<TEnitty> where TEnitty:AggregateRoot
    {
    }
}
