﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Enum
{
    public enum CommandEnum
    {
        Insert,
        Update,
        Delete
    }
}
