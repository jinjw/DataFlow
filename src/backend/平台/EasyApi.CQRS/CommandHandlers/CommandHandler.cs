﻿using EasyApi.CQRS.Command;
using EasyApi.CQRS.Domain;
using EasyApi.CQRS.Entity;
using EasyApi.CQRS.Enum;
using EasyApi.CQRS.Events;
using EasyApi.CQRS.Repositoies;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.CommandHandlers
{
    public class CommandHandler<T> :ICommandHandler<T> where T : AggregateRoot
    {
        private IDomainRepository<T> _domainRepository;

        public CommandHandler(IDomainRepository<T> domainRepository) {
            _domainRepository = domainRepository;
        }


        public void Execute(Command<T> command) 
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }
            //本次操作之前的聚合根数据
            T aggregateRoot = null;
            //如果是新增,只需要保存当前聚合根
            if (command.CommandEnum == CommandEnum.Insert)
            {
                aggregateRoot = command.AggregateRoot;
                //领域根中的所有的对象
                aggregateRoot.ApplyChange(new DomainEvent(aggregateRoot.Id,EventEnum.Insert,aggregateRoot.Version, command.AggregateRoot.MainEntity));
                foreach (BaseEntity baseEntity in command.AggregateRoot.Entities)
                {
                    aggregateRoot.ApplyChange(new DomainEvent(aggregateRoot.Id, EventEnum.Insert, aggregateRoot.Version, baseEntity));
                }
            }
            else if (command.CommandEnum == CommandEnum.Update)
            {
                aggregateRoot = _domainRepository.GetById(command.Id);
                //得到Command中的实体数据与聚合根进行对比
                aggregateRoot.Compare(command.AggregateRoot);
            }
            else if (command.CommandEnum == CommandEnum.Delete) {
                aggregateRoot.ApplyChange(new DomainEvent(aggregateRoot.Id, EventEnum.Delete, aggregateRoot.Version, command.AggregateRoot.MainEntity));
                foreach (BaseEntity baseEntity in command.AggregateRoot.Entities)
                {
                    aggregateRoot.ApplyChange(new DomainEvent(aggregateRoot.Id, EventEnum.Delete, aggregateRoot.Version, baseEntity));
                }
            }
            //命令转聚合根
            _domainRepository.Save(aggregateRoot);
        }
    }
}
