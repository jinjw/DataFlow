﻿using EasyApi.CQRS.Command;
using EasyApi.CQRS.Domain;
using EasyApi.CQRS.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace EasyApi.CQRS.CommandHandlers
{
    public interface ICommandHandler<T> where T : AggregateRoot
    {
        void Execute(Command<T> command) ;
    }
}
