﻿using EasyApi.CQRS.Command;
using EasyApi.CQRS.Domain;
using EasyApi.CQRS.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Bus
{
    public interface ICommandBus
    {
        void Send<T>(Command<T> command) where T:AggregateRoot;
    }
}
