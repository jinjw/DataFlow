﻿using EasyApi.CQRS.Command;
using EasyApi.CQRS.CommandHandlers;
using EasyApi.CQRS.Domain;
using EasyApi.CQRS.Entity;
using EasyApi.CQRS.Events.Storage;
using EasyApi.CQRS.Repositoies;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyApi.CQRS.Bus
{
    public class CommandBus : ICommandBus
    {
        public void Send<T>(Command<T> command) where T:AggregateRoot
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }
            IEventStore eventStore = new EventStore();
            IDomainRepository<T> domainRepository = new DomainRepository<T>(eventStore);
            ICommandHandler<T> handler = new CommandHandler<T>(domainRepository);
            handler.Execute(command);
        }
    }
}
